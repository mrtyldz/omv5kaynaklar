# omv5kaynaklar

https://t.me/openmediavaulttr  <- telegram grubum

https://docs.docker.com/config/pruning/  <- docker artıklarını temizlemek için

https://phoenixnap.com/kb/linux-network-speed-test#htoc-using-cmb-to-show-network-speed  <- ağ hızını test etmek isterseniz

https://github.com/Qballjos/portainer_templates  <- app templates için

https://wiki.omv-extras.org/doku.php?id=installing_omv5_raspberry_pi   <- omv5 kurulum 

https://docs.linuxserver.io/faq   <- option3 kurulumu

https://github.com/crazy-max/docker-cloudflared  <- docker arm cloudflared için

https://hub.docker.com/u/linuxserver/   <-docker app templates yerine elle girmek için

https://forum.openmediavault.org/   <- omv forumu

https://docs.docker.com/network/macvlan/  <-macvlan için
